﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuDisplay : MonoBehaviour
{
    [SerializeField] RectTransform SelfRect;
    bool Escondido = true;
    [SerializeField] float HideX = 0;
    float largura = 1000;
    public void OnClick()
    {
        if (Escondido == true)
        {
            Escondido = false;
            SelfRect.anchoredPosition = new Vector2(HideX, SelfRect.anchoredPosition.y);
        }
        else
        {
            Escondido = true;
            SelfRect.anchoredPosition = new Vector2(HideX - largura, SelfRect.anchoredPosition.y);
        }
    }
}
