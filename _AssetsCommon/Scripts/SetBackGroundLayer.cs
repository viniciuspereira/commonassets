﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBackGroundLayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(Delay());
	}
	
	IEnumerator Delay()
    {
        yield return new WaitForSeconds(1);
        GetComponentInChildren<Vuforia.BackgroundPlaneBehaviour>().gameObject.layer = 9;
    }
}
