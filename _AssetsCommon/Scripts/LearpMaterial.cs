﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearpMaterial : MonoBehaviour
{

    public string proprietName;

    private bool startLearp;
    private MeshRenderer meshRenderer;

    private float timer = 0;

    [ContextMenu("t")]
    public void StartLearp()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        startLearp = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (startLearp && timer<1)
        {
            meshRenderer.material.SetFloat(proprietName, Mathf.Lerp(1, 0, timer));
            timer += Time.deltaTime;
        }
        else
        {
            startLearp = false;
            timer = 0;
        }
    }
}
